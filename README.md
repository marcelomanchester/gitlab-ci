# Setting up Docker-based CI/CD 


To ensure that all tests run in an environment similar to the production environment and the integrity of sensitive data for connection to other remote servers, you must configure your own task executor with gitlab-runner.